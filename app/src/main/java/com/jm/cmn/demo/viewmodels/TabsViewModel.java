package com.jm.cmn.demo.viewmodels;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.jm.cmn.demo.adapters.ViewPagerAdapter;
import com.jm.cmn.demo.views.fragments.Fragment1;
import com.jm.cmn.demo.views.fragments.Fragment2;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractTabsViewModel;

/**
 * Created by Jakub Muran on 22.09.2017.
 * CoverPage s.r.o.
 */

public class TabsViewModel extends AbstractTabsViewModel {

    @Override
    protected PagerAdapter createAdapter() {
        return new ViewPagerAdapter(((AppCompatActivity) getView()).getSupportFragmentManager());
    }

    @Override
    public void addTabs(Bundle args) {
        addTab("Fragment1", "Fragment1", Fragment1.class, args);
        addTab("Fragment2", "Fragment2", Fragment2.class, args);
    }

    public TabsViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
