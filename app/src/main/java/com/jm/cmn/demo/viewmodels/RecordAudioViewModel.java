package com.jm.cmn.demo.viewmodels;

import android.databinding.Bindable;
import android.view.View;

import com.jm.cmn.demo.BR;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.managers.AudioPlayer;
import com.jm.cmn.managers.AudioRecorder;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 19.09.2017.
 */

public class RecordAudioViewModel extends AbstractViewModel implements AudioRecorder.OnStateChangedListener, AudioRecorder.OnRecordingTimeElapsedListener, AudioPlayer.OnStateChangedListener {

    private AudioRecorder mRecorder;

    private AudioPlayer mPlayer;

    @Bindable
    public AudioRecorder.State getState() {
        return this.mRecorder.getState();
    }

    @Bindable
    public AudioPlayer.State getPlayerState() { return this.mPlayer.getState(); }

    @Bindable
    public long getElapsedRecordingTime() {
        return this.mRecorder.getRecordingTimeElapsed();
    }

    public RecordAudioViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
        String path = view.getContext().getExternalCacheDir().getAbsolutePath();
        path += "/audio.3gp";
        this.mRecorder = new AudioRecorder(path);
        this.mRecorder.registerOnStateChangedListener(this);
        this.mRecorder.registerOnRecordingTimeElapsedListener(this);

        this.mPlayer = new AudioPlayer(path);
        this.mPlayer.registerOnStateChangedListener(this);
    }

    @Override
    public void onStateChanged(AudioRecorder.State oldState, AudioRecorder.State newState) {
        notifyPropertyChanged(BR.state);
    }

    @Override
    public void onRecordingTimeElapsed(long recordingTimeElapsed) {
        notifyPropertyChanged(BR.elapsedRecordingTime);
    }

    @Override
    public void onStateChanged(AudioPlayer.State oldState, AudioPlayer.State newState) {
        notifyPropertyChanged(BR.playerState);
    }

    public void onRecordButtonClickEvent(View view) {
        if(this.mRecorder.getState() == AudioRecorder.State.IDLE) this.mRecorder.startRecording();
        else this.mRecorder.stopRecording();
    }

    public void onPlayButtonClickEvent(View view) {
        if(this.mPlayer.getState() == AudioPlayer.State.IDLE) this.mPlayer.startPlaying();
        else this.mPlayer.stopPlaying();
    }

}
