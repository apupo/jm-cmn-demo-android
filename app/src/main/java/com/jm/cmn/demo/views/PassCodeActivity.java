package com.jm.cmn.demo.views;

import android.databinding.ViewDataBinding;
import android.os.Bundle;

import com.jm.cmn.demo.BR;
import com.jm.cmn.demo.R;
import com.jm.cmn.demo.viewmodels.PassCodeComponentActivityViewModel;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.activities.AbstractCameraActivity;

public class PassCodeActivity extends AbstractCameraActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindContentView(R.layout.pass_code_component_activity, new PassCodeComponentActivityViewModel(mStoreProvider, this));
        setTitle("Pass code component");
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }

}
