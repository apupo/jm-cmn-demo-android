package com.jm.cmn.demo.views;

import android.databinding.ViewDataBinding;
import android.os.Bundle;

import com.jm.cmn.demo.BR;
import com.jm.cmn.demo.viewmodels.MainActivityViewModel;
import com.jm.cmn.demo.R;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.activities.AbstractActivity;

public class MainActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Demo");
        bindContentView(R.layout.main_activity_layout, new MainActivityViewModel(mStoreProvider, this));
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }
}
