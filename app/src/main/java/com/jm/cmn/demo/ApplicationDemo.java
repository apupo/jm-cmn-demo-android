package com.jm.cmn.demo;

import com.jm.cmn.AbstractApplication;

/**
 * Created by Jakub Muran on 21.08.2017.
 */
public class ApplicationDemo extends AbstractApplication {

    @Override
    protected String makeSharedPreferencesKey() {
        return "JM_CMN_DEMO_SHARED_PREFERENCES_KEY";
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }
}
