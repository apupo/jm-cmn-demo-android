package com.jm.cmn.demo.viewmodels;

import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 22.09.2017.
 */

public class Fragment2ViewModel extends AbstractViewModel {

    public Fragment2ViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
