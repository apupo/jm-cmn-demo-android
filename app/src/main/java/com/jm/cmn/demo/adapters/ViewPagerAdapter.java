package com.jm.cmn.demo.adapters;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.jm.cmn.demo.views.fragments.Fragment1;
import com.jm.cmn.demo.views.fragments.Fragment2;
import com.jm.cmn.views.fragments.AbstractFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 22.09.2017.
  */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public enum FragmentsType implements Parcelable {
        FRAGMENT1, FRAGMENT2;

        public static final String PARCELABLE_KEY = "fragments_type_parcelable_key";

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(ordinal());
        }

        public static final Creator<FragmentsType> CREATOR = new Creator<FragmentsType>() {

            @Override
            public FragmentsType createFromParcel(Parcel source) {
                return values()[source.readInt()];
            }

            @Override
            public FragmentsType[] newArray(int size) {
                return new FragmentsType[size];
            }
        };

        public Bundle makeBundle() {
            Bundle bundle = new Bundle();
            bundle.putParcelable(PARCELABLE_KEY, this);
            return bundle;
        }

    }

    private List<FragmentsType> types;
    public List<FragmentsType> getTypes() {
        return types;
    }


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

        types = new ArrayList<>();
        types.add(FragmentsType.FRAGMENT1);
        types.add(FragmentsType.FRAGMENT2);

    }

    @Override
    public Fragment getItem(int position) {
        FragmentsType type = getTypes().get(position);

        AbstractFragment fragment = null;

        switch (type) {
            case FRAGMENT1:
                fragment = new Fragment1();
                break;
            case FRAGMENT2:
                fragment = new Fragment2();
                break;
        }

        fragment.setArguments(type.makeBundle());

        return fragment;
    }

    @Override
    public int getCount() {
        return getTypes().size();
    }
}
