package com.jm.cmn.demo.views;

import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.models.NavigationModel;
import com.jm.cmn.views.activities.AbstractNavigationActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 9/20/2017.
 */

public class NavigationActivity extends AbstractNavigationActivity {

    @Override
    public List<IAbstractRecyclerViewRendererData> createSource() {
        final List<IAbstractRecyclerViewRendererData> out = new ArrayList<>();
        out.add(new NavigationModel("JEDNA", 0, null));
        out.add(new NavigationModel("DVA", 0, null));
        out.add(new NavigationModel("TRI", 0, null));
        return out;
    }
}
