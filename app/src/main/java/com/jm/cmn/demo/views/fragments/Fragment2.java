package com.jm.cmn.demo.views.fragments;

import android.databinding.ViewDataBinding;
import android.view.View;

import com.jm.cmn.demo.BR;
import com.jm.cmn.demo.R;
import com.jm.cmn.demo.viewmodels.Fragment2ViewModel;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.fragments.AbstractFragment;

/**
 * Created by Jakub Muran on 22.09.2017.
 * CoverPage s.r.o.
 */

public class Fragment2 extends AbstractFragment {

    @Override
    protected int getLayoutResID() {
        return R.layout.fragment2_layout;
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    protected AbstractViewModel instantiateViewModel() {
        return new Fragment2ViewModel(getStoreProvider(), (IAbstractView) getActivity());
    }

    @Override
    protected void onCreateViewCompleted(View root) {

    }
}
