package com.jm.cmn.demo.viewmodels;

import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 22.09.2017.
 */

public class Fragment1ViewModel extends AbstractViewModel {

    public Fragment1ViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
