package com.jm.cmn.demo.views;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.jm.cmn.demo.BR;
import com.jm.cmn.demo.R;
import com.jm.cmn.demo.viewmodels.RecordAudioViewModel;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 19.09.2017.
 */

public class RecordAudioActivity extends AbstractActivity {

    private RecordAudioViewModel mViewModel;

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private String[] mPermissions = {Manifest.permission.RECORD_AUDIO};
    private boolean mPermissionToRecordAudioAccepted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mViewModel = new RecordAudioViewModel(mStoreProvider, this);
        setTitle("Record audio");
        bindContentView(R.layout.record_audio_activity, this.mViewModel);

        ActivityCompat.requestPermissions(this, this.mPermissions, REQUEST_RECORD_AUDIO_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                this.mPermissionToRecordAudioAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if(!this.mPermissionToRecordAudioAccepted) finish();
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }
}
