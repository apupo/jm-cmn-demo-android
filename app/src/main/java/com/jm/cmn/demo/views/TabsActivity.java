package com.jm.cmn.demo.views;

import android.databinding.ViewDataBinding;
import android.os.Bundle;

import com.jm.cmn.demo.BR;
import com.jm.cmn.demo.R;
import com.jm.cmn.demo.viewmodels.TabsActivityViewModel;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.views.activities.AbstractActivity;

/**
 * Created by Jakub Muran on 22.09.2017.
 */
public class TabsActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindContentView(R.layout.tabs_activity_main, new TabsActivityViewModel(mStoreProvider, this));
    }

    @Override
    protected void configureDataBindingVariables(ViewDataBinding viewDataBinding, AbstractViewModel viewModel) {
        viewDataBinding.setVariable(BR.viewModel, viewModel);
    }
}
