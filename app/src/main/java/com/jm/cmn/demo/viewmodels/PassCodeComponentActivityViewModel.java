package com.jm.cmn.demo.viewmodels;

import com.jm.cmn.demo.R;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;
import com.jm.cmn.viewmodels.PassCodeComponent;

/**
 * Created by Jakub Muran on 8/25/2017.
 */

public class PassCodeComponentActivityViewModel extends AbstractViewModel {

    public PassCodeComponentActivityViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }

    private PassCodeComponent mPassCodeComponent;
    public PassCodeComponent getPassCodeComponent() {
        if(this.mPassCodeComponent == null) {
            this.mPassCodeComponent = new PassCodeComponent(getView().getContext(), R.string.pass_code_component_title);
        }

        return this.mPassCodeComponent;
    }
}
