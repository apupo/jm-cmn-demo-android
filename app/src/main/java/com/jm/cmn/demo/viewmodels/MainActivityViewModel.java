package com.jm.cmn.demo.viewmodels;

import android.view.View;

import com.jm.cmn.demo.views.NavigationActivity;
import com.jm.cmn.demo.views.PassCodeActivity;
import com.jm.cmn.demo.views.PhotosComponentActivity;
import com.jm.cmn.demo.views.RecordAudioActivity;
import com.jm.cmn.demo.views.TabsActivity;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 21.08.2017.
 */
public class MainActivityViewModel extends AbstractViewModel {

    public MainActivityViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }

    public void onPhotosComponentClickEvent(View view) {
        getView().navigateTo(PhotosComponentActivity.class);
    }

    public void onPassCodeComponentClickEvent(View view) {
        getView().navigateTo(PassCodeActivity.class);
    }

    public void     onNavigationClickEvent(View view) {
        getView().navigateTo(NavigationActivity.class);
    }

    public void onRecordAudioClickEvent(View view) {
        getView().navigateTo(RecordAudioActivity.class);
    }

    public void onTabsClickEvent(View view) {
        getView().navigateTo(TabsActivity.class);
    }

}
