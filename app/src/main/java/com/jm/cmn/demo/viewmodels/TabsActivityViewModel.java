package com.jm.cmn.demo.viewmodels;

import android.databinding.Bindable;

import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.viewmodels.AbstractViewModel;

/**
 * Created by Jakub Muran on 22.09.2017.
 */

public class TabsActivityViewModel extends AbstractViewModel {

    private TabsViewModel mTabsViewModel;
    @Bindable
    public TabsViewModel getTabsViewModel() {
        if(this.mTabsViewModel == null) this.mTabsViewModel = new TabsViewModel(getStoreProvider(), getView());
        return this.mTabsViewModel;
    }

    public TabsActivityViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
    }
}
