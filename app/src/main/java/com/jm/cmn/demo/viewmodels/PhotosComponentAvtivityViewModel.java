package com.jm.cmn.demo.viewmodels;

import com.jm.cmn.adapters.PhotosRecyclerAdapter;
import com.jm.cmn.demo.R;
import com.jm.cmn.interfaces.IAbstractRecyclerViewRendererData;
import com.jm.cmn.interfaces.IAbstractView;
import com.jm.cmn.interfaces.ICameraView;
import com.jm.cmn.interfaces.IStoreProvider;
import com.jm.cmn.models.PhotosComponent;
import com.jm.cmn.viewmodels.AbstractViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jakub Muran on 24.08.2017.
 * CoverPage s.r.o.
 */
public class PhotosComponentAvtivityViewModel extends AbstractViewModel {

    private ICameraView mCameraView;

    private List<IAbstractRecyclerViewRendererData> mPhotos = new ArrayList<>();

    public PhotosComponentAvtivityViewModel(IStoreProvider storeProvider, IAbstractView view) {
        super(storeProvider, view);
        this.mCameraView = (ICameraView) view;
    }

    private PhotosComponent mPhotosComponent;
    public PhotosComponent getPhotosComponent() {
        if(this.mPhotosComponent == null) {
            final PhotosRecyclerAdapter adapter = new PhotosRecyclerAdapter(getView().getContext(), getStoreProvider());
            mPhotosComponent = new PhotosComponent(
                    R.string.photos_component_title,
                    -1,
                    adapter,
                    mPhotos,
                    getView());
        }

        return mPhotosComponent;
    }
}
